 interface Props {
        poster_path: string;
        overview: string;
        release_date: string;
        title: string;
        id: string;
}
const favoriteFilmsListMarkup = (filmsArr: []): string => filmsArr.map(({ poster_path, overview, release_date, id }: Props) => {
        
        return `<div class="col-12 p-2" >
                        <div class="card shadow-sm">
                                <img
                                    src="https://image.tmdb.org/t/p/original/${poster_path}"
                                />
                        <svg 
                            xmlns="http://www.w3.org/2000/svg"
                            stroke="red"
                            fill="red"
                            width="50"
                            height="50"
                            class="bi bi-heart-fill position-absolute p-2 marked-as-favourite"
                            viewBox="0 -2 18 22"
                        >
                            <path data-id="${id}"
                                fill-rule="evenodd"
                                d="M8 1.314C12.438-3.248 23.534 4.735 8 15-7.534 4.736 3.562-3.248 8 1.314z"
                            />
                        </svg>
                                <div class="card-body">
                                    <p class="card-text truncate">
                                        ${overview}
                                    </p>
                                    <div
                                        class="
                                            d-flex
                                            justify-content-between
                                            align-items-center
                                        "
                                    >
                                        <small class="text-muted">${release_date}</small>
                                    </div>
                                </div>
                </div>
            </div>`}).join(' ');

export default favoriteFilmsListMarkup;