export default interface Item {
        poster_path: string;
        backdrop_path: string;
        overview: string;
        release_date: string;
        id: string | number;
        title: string;
        }