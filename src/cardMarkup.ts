import Item from "./iMarkedItem";
 
export default function filmsListMarkup(filmsArr: Item[]): string {
    // 
  const arr =  filmsArr.map(({ poster_path, overview, release_date, id }: Item) => {
        const storedMarkedIcons = localStorage.getItem('favorite-marked') || '{}';
        const markedArr: string[] = JSON.parse(storedMarkedIcons);

        return `<div class="col-lg-3 col-md-4 col-12 p-2" >
                        <div class="card shadow-sm" data-id="${id}">
                                <img
                                    src=${poster_path ? `https://image.tmdb.org/t/p/original/${poster_path}` : "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSGo0LDj2YWAfB8ZQjfpJvLRKRDT4T-cMpdzg&usqp=CAU"}
                                />
                                <svg data-id="${id}" 
                                    xmlns="http://www.w3.org/2000/svg"
                                    stroke="red"
                                    fill="red"
                                    width="50"
                                    height="50"
                                    class="bi bi-heart-fill position-absolute p-2 ${markedArr.find(el => el == id) ? "marked-as-favourite" : ""}"
                                    viewBox="0 -2 18 22"
                                >
                                    <path data-id="${id}" 
                                        fill-rule="evenodd"
                                        d="M8 1.314C12.438-3.248 23.534 4.735 8 15-7.534 4.736 3.562-3.248 8 1.314z"
                                    />
                                </svg>
                                <div class="card-body">
                                    <p class="card-text truncate">
                                        ${overview}
                                    </p>
                                    <div
                                        class="
                                            d-flex
                                            justify-content-between
                                            align-items-center
                                        "
                                    >
                                        <small class="text-muted">${release_date}</small>
                                    </div>
                                </div>
                </div>
            </div>`}).join(' ');
  
    return arr;
}

// export default filmsListMarkup;