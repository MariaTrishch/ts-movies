const FAVORITE_FILMS_STORAGE = 'favorites-list';
const FAVORITE_FILMS_MARKED = 'favorite-marked';

export default function addToFavourites(e: any): void {
    interface Item {
        id: string;
    }
        const favCardObj = {
                poster_path: e.target.closest('.card').querySelector('img').getAttribute('src'),
                overview: e.target.closest('.card').querySelector('.card-text').textContent,
                release_date: e.target.closest('.card').querySelector('.text-muted').textContent,
                id: e.target.dataset.id,
        }

        if (e.target.parentNode.classList.contains('marked-as-favourite')) {
            e.target.parentNode.classList.remove('marked-as-favourite');

            const favouriteFilms = new Set(
            JSON.parse(localStorage.getItem(FAVORITE_FILMS_STORAGE) || '{}'),
            );
            
            const favouriteMarked = new Set(
            JSON.parse(localStorage.getItem(FAVORITE_FILMS_MARKED) || '{}'),
            );
            favouriteMarked.delete(e.target.dataset.id);
            
            const getFilmsFromStorage = Array.from(favouriteFilms) as Item[];
            const filmToDelete = getFilmsFromStorage.find(item => item.id === e.target.dataset.id);
            
            favouriteFilms.delete(filmToDelete);

            localStorage.setItem(
            FAVORITE_FILMS_STORAGE,
            JSON.stringify([...favouriteFilms.values()]),
            );
            localStorage.setItem(
            FAVORITE_FILMS_MARKED,
            JSON.stringify([...favouriteMarked.values()]),
            );
            
        } else {
            e.target.parentNode.classList.add('marked-as-favourite');
                      
            const favouriteFilms = new Set(
            JSON.parse(localStorage.getItem(FAVORITE_FILMS_STORAGE) || '{}'),
            );
            const favouriteMarked = new Set(
            JSON.parse(localStorage.getItem(FAVORITE_FILMS_MARKED) || '{}'),
            );
            
            const getFilmsFromStorage = Array.from(favouriteFilms) as Item[];
            const shouldAddFilm = getFilmsFromStorage.some(item => item.id === e.target.dataset.id);

            if (e.target.hasAttribute('data-id') && !shouldAddFilm) {
                favouriteFilms.add(favCardObj);
                favouriteMarked.add(e.target.dataset.id);
            }

            localStorage.setItem(
            FAVORITE_FILMS_STORAGE,
            JSON.stringify([...favouriteFilms.values()]),
            );
            localStorage.setItem(
            FAVORITE_FILMS_MARKED,
            JSON.stringify([...favouriteMarked.values()]),
            );
           
        }
    }
