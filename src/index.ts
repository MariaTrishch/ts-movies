import cardsList from './cardsList';
import DataApiService from './apiService';
import getRefs from './get-refs';
import addToFavourites from './favouriteStorage';
import getFilmData from './search';
import favoriteFilmsListMarkup from './favoriteCardMarkup';

export async function render(): Promise<void> {
  
    const refs = getRefs();
    const dataApiService = new DataApiService();

    function check(e: {target:HTMLInputElement}) {
        e.target.checked = true;
        refs.filmContainerEl.innerHTML = '';

        dataApiService.resetPage();
        getData();
    }
    refs.buttonsWrapperEl.addEventListener('change', e => check(e));
       
    async function getData() {
        interface Item {
        poster_path: string;
        backdrop_path: string;
        overview: string;
        release_date: string;
        id: string | number;
        title: string;
        }
        const radioBtn = document.querySelector('[type="radio"]:checked') as HTMLElement;
        
        if (!radioBtn) {
            const fetchedFilmsArr = await dataApiService.fetchFilm(refs.searchInputEl.value);
            const fetchedFilmsData = fetchedFilmsArr.results as Item[];
            const filmsArr = fetchedFilmsData.map(filmObj => { return { id: filmObj.id, poster_path: filmObj.poster_path, backdrop_path: filmObj.backdrop_path, overview: filmObj.overview, release_date: filmObj.release_date, title: filmObj.title } });

            return cardsList(filmsArr);
        } else {
            const category = radioBtn.getAttribute('id') as string;
            const fetchedFilmsArr = await dataApiService.fetchData(category);
            const fetchedFilmArr = fetchedFilmsArr.results as Item[];
            const filmsArr = fetchedFilmArr.map(filmObj => { return { id: filmObj.id, poster_path: filmObj.poster_path, backdrop_path: filmObj.backdrop_path, overview: filmObj.overview, release_date: filmObj.release_date, title: filmObj.title } });

        return cardsList(filmsArr);
        }
    }
    getData();


    const favoriteArr = JSON.parse(localStorage.getItem('favorites-list') || '{}');
 
    if (favoriteArr) {
        refs.favoriveFilmsWrapperEl.insertAdjacentHTML('beforeend', favoriteFilmsListMarkup(favoriteArr));
    } 
    refs.favoriveFilmsWrapperEl.insertAdjacentHTML('beforeend', '<div>Empty</div>');

    refs.loadMoreBtnEl.addEventListener('click', getData);
    refs.filmContainerEl.addEventListener('click', e => addToFavourites(e));
    refs.favoriveFilmsWrapperEl.addEventListener('click', e => addToFavourites(e));
    refs.searchSubmitBtnEl.addEventListener('click', getFilmData);
}
