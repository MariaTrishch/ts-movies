export default function getRefs() {

    return {
        filmContainerEl: document.querySelector('#film-container') as HTMLElement,
        buttonsWrapperEl: document.querySelector('#button-wrapper') as HTMLElement,
        popularFilmsInputEl: document.querySelector('#popular') as HTMLElement,
        upcomingFilmsInputEl: document.querySelector('#upcoming') as HTMLElement,
        topRatedFilmsInputEl: document.querySelector('#top_rated') as HTMLElement,
        searchInputEl: document.querySelector('#search') as HTMLInputElement,
        searchSubmitBtnEl: document.querySelector('#submit') as HTMLElement,
        loadMoreBtnEl: document.querySelector('#load-more') as HTMLElement,
        sectionRandomMovie: document.querySelector('#random-movie') as HTMLElement,
        sectionRandomMovieTitle: document.querySelector('#random-movie-name') as HTMLElement,
        sectionRandomMovieText: document.querySelector('#random-movie-description') as HTMLElement,
        favoriveFilmsWrapperEl: document.querySelector('#favorite-movies') as HTMLElement,
        inputPopular: document.querySelector('#popular') as HTMLInputElement,
        inputUpcoming: document.querySelector('#upcoming') as HTMLInputElement,
        inputTopRated: document.querySelector('#top_rated') as HTMLInputElement,
    };
}