const BASE_URL = 'https://api.themoviedb.org/3/';
const API_KEY = '726653b8cacb73d155407508fdc35e60';
export default class DataApiService {
    page: number;
   
      constructor() {
          this.page = 1;
    }

    async fetchData(category: string): Promise<any> {
        const response = await fetch(`${BASE_URL}movie/${category}?api_key=${API_KEY}&page=${this.page}`);
        const filmsData = await response.json();

        this.page += 1;
      
        return filmsData;
    }

    async fetchFilm(query: string): Promise<any> {
        const response = await fetch(`${BASE_URL}search/movie?api_key=${API_KEY}&query=${query}&page=${this.page}&include_adult=false`);
        const searchedFilms = await response.json();
       
        this.page += 1;
        
        return searchedFilms;
    }

    resetPage(): void{
        this.page = 1;
    }

}