import getRefs from './get-refs';
import filmsListMarkup from './cardMarkup';
import Item from './iMarkedItem';

   
export default function cardsList(filmsArr: Item[]): void {
            
    const refs = getRefs();
    
    const randomInd = Math.ceil(Math.random() * filmsArr.length) + 1;
    const backdropImg = filmsArr[randomInd]?.backdrop_path;
    const randomTitle = filmsArr[randomInd]?.title;
    const randomDescription = filmsArr[randomInd]?.overview;
    refs.sectionRandomMovie.style.background = `url(https://image.tmdb.org/t/p/original/${backdropImg})`;
    refs.sectionRandomMovieTitle.textContent  = randomTitle;
    refs.sectionRandomMovieText.textContent  = randomDescription;
   
    refs.filmContainerEl.insertAdjacentHTML('beforeend', filmsListMarkup(filmsArr));

}