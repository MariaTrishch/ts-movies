import getRefs from './get-refs';
import cardsList from './cardsList';
import  DataApiService from './apiService';


const dataApiService = new DataApiService();
const refs = getRefs();

export default async function getFilmData(): Promise<any> {
  const radioBtn = <HTMLInputElement>document.querySelector('[type="radio"]:checked');
  radioBtn.checked = false;

      
  if (refs.searchInputEl.value.trim()) {
    const films = await dataApiService.fetchFilm(refs.searchInputEl.value);
    
    refs.filmContainerEl.innerHTML = '';
    refs.filmContainerEl.insertAdjacentHTML('beforeend', cardsList(films.results));
  }
}